from datetime import timedelta

import numpy as np
import pandas as pd
import pytest

from yl_data_api_client_wrapper.date_utils import ensure_datetime
from yl_data_api_client_wrapper.utils import (
    fill_in_missing_dates,
    response_to_dataframe,
)


@pytest.fixture
def date_from():
    return ensure_datetime("2021-01-01")


@pytest.fixture
def date_to():
    return ensure_datetime("2021-01-31")


def test_fill_in_missing_dates_extra_dates(date_from, date_to):
    dates = pd.date_range(date_from, date_to + timedelta(days=1))
    df = pd.DataFrame({"date": dates, "values": np.arange(len(dates))})

    df_new = fill_in_missing_dates(df, date_from, date_to, column="date")

    assert len(df_new) == (date_to - date_from).days + 1


def test_fill_in_missing_dates_tz_aware(date_from, date_to):
    dates = pd.date_range(date_from, date_to + timedelta(days=1)).tz_localize("utc")

    df = pd.DataFrame({"date": dates, "values": np.arange(len(dates))})

    df_new = fill_in_missing_dates(df, date_from, date_to, column="date")

    assert hasattr(df.date.dtype, "tz")
    assert len(df_new) == (date_to - date_from).days + 1


def test_fill_in_missing_dates_holey(date_from, date_to):
    dates = pd.date_range(date_from, date_to - timedelta(days=1)).tz_localize("utc")
    df = pd.DataFrame({"date": dates, "values": np.arange(len(dates))})
    df.drop([10, 15, 29], axis=0, inplace=True)

    df_new = fill_in_missing_dates(df, date_from, date_to, column="date")

    assert len(df_new) == (date_to - date_from).days + 1


def test_ensure_raises_error(date_from, date_to):
    dates = pd.date_range(date_from, date_to - timedelta(days=1)).tz_localize("utc")

    df = pd.DataFrame(index=dates, data={"values": np.arange(len(dates))})

    with pytest.raises(ValueError):
        fill_in_missing_dates(df, date_from, date_to)

    df = pd.DataFrame(
        {"date": dates.strftime("%Y-%m-%d"), "values": np.arange(len(dates))}
    )

    with pytest.raises(ValueError):
        fill_in_missing_dates(df, date_from, date_to)


def test_to_df(events_v3_response):
    df = response_to_dataframe(events_v3_response)
    assert df.publish_time.nunique() == 7
