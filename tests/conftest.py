import json
from pathlib import Path

import pytest


@pytest.fixture(scope="module")
def test_data():
    cur_dir = Path(__file__).parent
    return cur_dir / "test_data"


@pytest.fixture(scope="module")
def events_v3_response(test_data):
    with (test_data / "events_v3_response.json").open() as fp:
        return json.load(fp)
