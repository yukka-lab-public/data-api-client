# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import pandas as pd

from yl_data_api_client_wrapper.date_utils import ensure_datetime


def test_ensure_datetime_str():
    dt_str = "2021-01-07"

    dt = ensure_datetime(dt_str)

    assert dt.year == 2021
    assert dt.month == 1
    assert dt.day == 7


def test_ensure_datetime_pd_Timestamp():

    timestamp = pd.Timestamp(1513393355.5, unit="s")
    dt = ensure_datetime(timestamp)

    assert dt.year == 2017
    assert dt.month == 12
    assert dt.day == 16
    assert dt.hour == 3
    assert dt.minute == 2
