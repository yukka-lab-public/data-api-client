import json
from typing import Optional

from yl_data_api_client_wrapper.news_iterator import NewsIterator


class MockNewsResponse(object):
    def __init__(self, item: dict):
        self.__dict__.update(**item)


def test_news_iterator(test_data):
    news_responses = [
        json.loads(p.read_text()) for p in test_data.glob("response_news*.json")
    ]
    news_responses = iter(sorted(news_responses, key=lambda r: r["news_total_count"]))

    def get_news_fn(news_after: Optional[str] = None):
        return MockNewsResponse(next(news_responses))

    news_iterator = NewsIterator.init(get_news_fn)
    initial_count = news_iterator.news_total_count
    news_items = [next(news_iterator) for i in range(40)]
    final_count = news_iterator.news_total_count
    assert len(news_items) == 40
    assert final_count > initial_count
