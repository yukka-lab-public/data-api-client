# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
"""
YUKKA Lab's Data API wrapper - companion package for wrangling responses from the Data API.
It contains various utility functions to transform responses into workable output formats.

## Installation

To use the DatA API wrapper a prerequisite is that you have obtained data from the Data API either using `curl` or the Data API client.
Next, you can install the wrapper from our registry;

```
pip3 install -i https://gitlab.com/api/v4/projects/27650067/packages/pypi/simple yl-data-api-client-wrapper
```

Please note that the `yl-data-api-client-wrapper` requires you to use `Python >= 3.8`-

Once you're set up, you're ready to send requests to the API:

```python
import os
import yl_data_api_client
from yl_data_api_client.rest import ApiException
from yl_data_api_client_wrapper import response_to_dataframe

if __name__ == "__main__":
    configuration = yl_data_api_client.Configuration(
        host="https://api.yukkalab.com/api-data/dev/9-3-5/",
        access_token=os.environ["BEARER_TOKEN"]
    )

    # Enter a context with an instance of the API client
    with yl_data_api_client.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = yl_data_api_client.EventsApi(api_client)
        input_model = yl_data_api_client.InputModel(
            entities=["company:tesla_motors"],
            inner_entities=["city:munich"]
        )

        try:
            # Fetch events
            api_response = api_instance.events_v4_api_events_post(input_model)
            # Convert to DataFrame
            df = response_to_dataframe(api_response)
            print(df.head())
            print(df["count"].sum())
        except ApiException as e:
            print("Could not fetch response")
```


The client documentation for the `yl_data_api_client_wrapper.YLDataAPIClient` goes into detail on all available methods.

## Examples

### Event counts

Let's get event counts for Tesla Motors for all of 2020.

```python
from yl_data_api_client_wrapper import YLDataAPIClient

client = YLDataAPIClient(token="ey...")
counts = client.api.company.tesla_motors.sub_events(
    date_from="2020-01-01", date_to="2021-01-01")

# get counts for a specific event:
counts = client.api.company.tesla_motors.sub_events(
    date_from="2020-01-01", date_to="2021-01-01", events=["E6_C"])
```

### News

Let's grab the news for Volkswagen for the last week (for larger companies like Volkswagen, this can result in *lots* of data):

```python
from tqdm import tqdm # optional, but useful!

news_instance = yl_data_api_client.NewsApi(api_client)
get_news_fn = partial(news_instance.news_v2_api_displayable_type_displayable_id_news_get,
    displayable_type="company",
    displayable_id="volkswagen",
)
iterator = NewsIterator(get_news_fn)

for article in tqdm(iterator, total=iterator.news_total_count):
   print(article)
```

### Timeseries endpoints

The YUKKA data API has several "timeseries" endpoints; these return the histogram of some quantity of interest like sentiment, volume, or event counts.
`yl_data_api_client_wrapper` will automatically create a pandas dataframe for these endpoints (we can disable this behavior by passing `return_resp=True` to any of these endpoints):

```python
from datetime import datetime

from yl_data_api_client_wrapper import YLDataAPIClient

client = YLDataAPIClient(token="ey...")

sentiment_df = client.api.company.tesla_motors.quotient_ts(
    date_from="2020-01-01", date_to=datetime.now())

events_df = client.api.company.tesla_motors.events_ts(
    date_from="2020-01-01", date_to=datetime.now())

volume_resp = client.api.company.tesla_motors.volume_ts(
    date_from="2020-01-01", date_to=datetime.now(), return_resp=True)  # return the requests.Response object for this request
```
"""

from yl_data_api_client_wrapper.news_iterator import NewsIterator  # noqa: F401
from yl_data_api_client_wrapper.utils import (  # noqa: F401
    fill_in_missing_dates,
    response_to_dataframe,
)
