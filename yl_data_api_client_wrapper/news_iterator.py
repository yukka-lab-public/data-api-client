# Copyright (C) 2023 YUKKA Lab AG, Berlin - All Rights Reserved
from dataclasses import dataclass, field
from itertools import islice
from typing import Any, Iterator, List, Optional, Protocol, Sequence, Union


class NewsLike:
    news: Sequence[Any]
    news_after: str
    news_total_count: int
    sentiment_counts: Any


class NewsAfterCallable(Protocol):
    def __call__(self, news_after: Optional[str]) -> NewsLike:
        ...


@dataclass
class NewsIterator:
    """
    Convenient iterator over the news endpoint.
    This can be used as a cursor for scrolling the Data API.

    ```python
    from itertools import islice

    import os
    import yl_data_api_client
    from yl_data_api_client_wrapper import NewsIterator
    from functools import partial

    configuration = yl_data_api_client.Configuration(
        host="https://api.yukkalab.com/api-data/dev/9-3-5/",
        access_token=os.environ["BEARER_TOKEN"]
    )

    # Enter a context with an instance of the API client
    with yl_data_api_client.ApiClient(configuration) as api_client:
        news_instance = yl_data_api_client.NewsApi(api_client)

        get_news_fn = partial(
            news_instance.news_v2_api_displayable_type_displayable_id_news_get,
            "city",
            "berlin"
        )
        print("Fetching first batch of news")
        iterator = NewsIterator.init(get_news_fn)
        print(f"{iterator.news_total_count} were written about Berlin in the past week.")
        # get the first 10 elements from the iterator
        articles = list(islice(iterator, 10))

        for article in articles:
            print(article.title)
    ```
    """

    get_news_fn: NewsAfterCallable = field(repr=False)
    news: list[dict] = field(repr=False)
    news_after: str
    news_total_count: int
    sentiment_counts: dict
    n_getitem: int = 0
    _iterator: Iterator[dict] = field(init=False)

    def __len__(self):
        return self.news_total_count

    def __iter__(self):
        yield from self.news
        counter = len(self.news)
        while counter < self.news_total_count:
            data = self.get_news_fn(news_after=self.news_after)
            self.news = data.news
            self.news_after = data.news_after
            self.news_total_count = data.news_total_count
            self.sentiment_counts = data.sentiment_counts
            yield from self.news
            counter += len(self.news)

    def __getitem__(self, item: Union[slice, int]) -> Union[dict, List[dict]]:
        if self.n_getitem > 0:
            raise StopIteration()
        if isinstance(item, int):
            results = list(islice(self, item + 1))
            if len(results) < item:
                raise IndexError("Index out of bounds!")
            results = results[-1]
        elif isinstance(item, slice):
            results = list(islice(self, item.start, item.stop, item.step))
        else:
            raise ValueError("need to pass either int or slice to __getitem__")
        self.n_getitem += 1
        return results

    def __next__(self):
        return next(self._iterator)

    def __post_init__(self):
        self._iterator = iter(self)

    @classmethod
    def init(cls, get_news_fn):
        data = get_news_fn()
        news_total_count = data.news_total_count
        return cls(
            get_news_fn,
            data.news,
            data.news_after,
            news_total_count,
            data.sentiment_counts,
        )
