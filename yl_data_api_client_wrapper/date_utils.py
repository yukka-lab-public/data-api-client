# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
from datetime import datetime
from typing import Union

import pandas as pd
from dateutil.parser import parse

__all__ = ["Date", "ensure_datetime"]


Date = Union[str, datetime, pd.Timestamp]


def ensure_datetime(dt: Date, **kwargs) -> datetime:
    if isinstance(dt, pd.Timestamp):
        return pd.to_datetime(dt)
    elif isinstance(dt, datetime):
        return dt
    elif hasattr(dt, "format"):
        return parse(dt, **kwargs)
    else:
        raise ValueError(f"Couldn't parse {dt} of type {type(dt)}")
