# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
from datetime import date
from typing import List, TypedDict, Union

import pandas as pd

from yl_data_api_client_wrapper.date_utils import Date, ensure_datetime


def has_tz(obj):
    return hasattr(obj.dtype, "tz")


def fill_in_missing_dates(
    df: pd.DataFrame, date_from: Date, date_to: Date, column: str = "publish_time"
):
    """
    Takes a DataFrame indexed by a DateTime and fills all values between
    date_from and date_to *inclusive* with NaN, if they are not contained in the original datasets.
    The assumed interval is days.
    """
    if column not in df:
        raise ValueError(f'df parameter needs to have "{column}" column')

    date_from = ensure_datetime(date_from)
    date_to = ensure_datetime(date_to)
    dtr = pd.date_range(date_from, date_to, freq="D")

    # if we use tz localized date field, then use this in our new index as well
    if has_tz(df[column]):
        dtr = dtr.tz_localize(df[column].dtype.tz)  # type: ignore

    if has_tz(dtr) and not has_tz(df[column]):
        df[column] = df[column].tz_localize(dtr.dtype.tz)  # type: ignore

    s = pd.DataFrame(index=dtr)
    s = s.reset_index()
    s[column] = s["index"]
    s = s.drop(["index"], axis=1)
    # when we merge on left, we're ensuring that only dates in the `s` dataframe are used
    df[column] = df[column].astype(s[column].dtype)  # type: ignore
    df = pd.merge(s, df, how="left", on=[column])
    return df


class EventResponse(TypedDict):
    events: List[List[Union[float, int, str, date]]]
    columns: List[str]


def response_to_dataframe(data: EventResponse) -> pd.DataFrame:
    """
    Utility function which converts a response to a DataFrame.
    @param data: A dictionary containing the numeric response and associated columns.
    @return: DataFrame

    """
    return pd.DataFrame(data["events"], columns=data["columns"])
