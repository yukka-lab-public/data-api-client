# Data API Client

You can find [the documentation](https://yukka-lab-public.gitlab.io/data-api-client/) online.

## Installation 

```
pip install -i https://gitlab.com/api/v4/projects/27650067/packages/pypi/simple yl-data-api-client-wrapper
```

## Usage

```python
from datetime import datetime, timedelta

import tqdm
from yl_data_api_client_wrapper import YLDataAPIClient

client = YLDataAPIClient()

articles = []

today = datetime.now()

params = dict(date_from=today - timedelta(days=7), date_to=today)
for article in tqdm.tqdm(client.api.company.tesla_motors.news(size=50, **params)):
  articles.append(article)

tesla_client = client.api.company.tesla_motors
events_df = tesla_client.events_ts(**params)
quotient_df = tesla_client.quotient_ts(**params)
```
