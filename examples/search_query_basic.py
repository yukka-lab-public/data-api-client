# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import datetime
from itertools import islice

from getpass import getpass
from yl_data_api_client_wrapper import YLDataAPIClient

print("Creating new client")
client = YLDataAPIClient(token=getpass(), api_version="7-2")

# selects displayable type SearchQuery
search_query = {
    "operator": "or",
    "filters": [{
        "negated": False,
        "field": "text",
        "name": None,
        "value": "My freetext search"
    }],
    "groups": []
}

search_query_client = client.api.search_query
now = datetime.datetime.now()
a_week_ago = now - datetime.timedelta(days=7)

print("Fetching first batch of news")
sq_news = search_query_client.news(
    date_from=a_week_ago,
    date_to=now,
    data=search_query
)

print(f"{sq_news.news_total_count} were written for the SearchQuery in the past week.")

# get the first 10 elements from the iterator
articles = list(islice(sq_news, 10))

for article in articles:
    print(article["title"])
