# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import datetime
from getpass import getpass

from yl_data_api_client_wrapper import YLDataAPIClient
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

print("Creating new client")
client = YLDataAPIClient(token=getpass(), api_version="7-2")

# selects displayable type city, and displayable name berlin
tesla_client = client.api.company.tesla_motors
# first 10 days in 2018
date_from = datetime.datetime.strptime("2018-01-01", "%Y-%m-%d")
date_to = date_from + datetime.timedelta(days=9)

print("Fetching sentiment for Tesla Motors")
sentiment_counts = tesla_client.sentiment_counts_ts(
  date_from=date_from,
  date_to=date_to
)
print(sentiment_counts.head())

sns.lineplot(x='date', y='value', hue='variable', 
             data=pd.melt(sentiment_counts, ['date']))
plt.savefig("sentiment_counts.png")
