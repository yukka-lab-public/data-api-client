# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import datetime

from getpass import getpass
from yl_data_api_client_wrapper import YLDataAPIClient

print("Creating new client")
client = YLDataAPIClient(token=getpass(), api_version="7-2")

# selects displayable type city, and displayable name berlin
berlin_client = client.api.city.berlin
now = datetime.datetime.now()
two_week_ago = now - datetime.timedelta(days=14)

print("Fetching stories for Berlin in the last two weeks")
berlin_stories = berlin_client.stories(
    date_from=two_week_ago,
    date_to=now
)

print(f"Found {len(berlin_stories['stories'])} story clusters for Berlin in the selected time frame")

for story in berlin_stories["stories"]:
    print(story["main_article"]["title"])
