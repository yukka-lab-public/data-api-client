# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import datetime
from getpass import getpass
from random import choices

from yl_data_api_client_wrapper import YLDataAPIClient
import pandas as pd

print("Creating new client")
client = YLDataAPIClient(token=getpass(), api_version="7-2")

portfolio = ["company:bosch", "city:berlin"]

# selects displayable type city, and displayable name berlin
berlin_client = client.api.portfolio(portfolio)
now = datetime.datetime.now()
a_week_ago = now - datetime.timedelta(days=14)

# define a list of events
events = [
    "E_32", "E_33", "E_34", "E_10", "E_41", "E_14", "E_9", "E_29", "E_28",
    "E_23", "E_24", "E_17", "E_4", "E_35", "E_26", "E_30", "E_31", "E_15",
    "E_37", "E_20", "E_42", "E_18", "E_16", "E_38", "E_40", "E_13", "E_27",
    "E_11", "E_22", "E_1", "E_2", "E_3", "E_8", "E_19", "E_7", "E_6", "E_0",
    "E_43", "E_25", "E_12", "E_36", "E_21", "E_5", "E_39"
]

events_to_pick = choices(events, k=10)

stories_for_event = []

for event in events_to_pick:
    print(f"Obtaining stories for event {event}")
    berlin_stories = berlin_client.stories(date_from=a_week_ago, date_to=now, event_ids=[event])
    # filter out "empty" stories
    stories = [story for story in berlin_stories["stories"] if "main_article" in story]

    for story in stories:
        title = story["main_article"]["title"]
        stories_for_event.append((event, title))

df = pd.DataFrame(stories_for_event, columns=["event", "title"])
print(df.groupby("event").head())
