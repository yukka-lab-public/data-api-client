# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import datetime
from itertools import islice
 
from getpass import getpass
from yl_data_api_client_wrapper import YLDataAPIClient
 
print("Creating new client")
client = YLDataAPIClient(token=getpass(), api_version="7-2")
 
# selects displayable type Portofolio and specifies multiple ISIN's
portfolio = ["isin:US64110L1061","isin:JP3435000009","isin:DE0007664039"]
 
print("Fetching score ts")
portfolio_scores = client.api.portfolio(portfolio).score_ts_itemized(
  date_from=datetime.datetime(2022, 1, 1),
  date_to=datetime.datetime(2022, 1, 2),
  score_type='e_standard'  # use the E_STANDARD score
)

# Returns a DataFrame for the scores
print(len(portfolio_scores))
print(portfolio_scores)
