# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
from itertools import chain, islice
from collections import Counter
import os
from functools import partial

import yl_data_api_client
from yl_data_api_client_wrapper import NewsIterator


def get_news():
    configuration = yl_data_api_client.Configuration(
        host="https://api.yukkalab.com/api-data/dev/9-3-5/",
        access_token=os.environ["BEARER_TOKEN"]
    )

    # Enter a context with an instance of the API client
    with yl_data_api_client.ApiClient(configuration) as api_client:
        news_instance = yl_data_api_client.NewsApi(api_client)

        get_news_fn = partial(
            news_instance.news_v2_api_displayable_type_displayable_id_news_get,
            "city",
            "berlin"
        )
        print("Fetching first batch of news")
        iterator = NewsIterator.init(get_news_fn)
        print(f"{iterator.news_total_count} were written about Berlin in the past week.")
        # get the first 100 elements from the iterator
        articles = list(islice(iterator, 100))

        print("Calculating word frequencies")
        titles = [article.title.encode('ascii', 'ignore').decode('ascii') for article in articles]

        # this splits each title into lists of words using whitespace
        # then accumulates all these lists into one list of words
        # a more robust approach is to use a tokenizer
        words = list(chain.from_iterable([title.split(" ") for title in titles]))

        word_frequency = Counter(words)
        print(word_frequency.most_common(10))

        # in natural language often times the most frequent items are not the interesting one
        # this is because of Zipf's law :)
        print("Printing the 25 least frequent words")
        print(word_frequency.most_common()[-25:])


if __name__ == "__main__":
    get_news()
