# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import logging
from datetime import datetime
import os

import pandas as pd
import yl_data_api_client
from yl_data_api_client.rest import ApiException
from yl_data_api_client_wrapper import response_to_dataframe, fill_in_missing_dates


def get_events() -> pd.DataFrame:
    logging.basicConfig(level=logging.DEBUG)
    configuration = yl_data_api_client.Configuration(
        host="https://api.yukkalab.com/api-data/dev/9-3-5/",
        access_token=os.environ["BEARER_TOKEN"]
    )

    # Enter a context with an instance of the API client
    with yl_data_api_client.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = yl_data_api_client.EventsApi(api_client)
        input_model = yl_data_api_client.InputModel(
            entities=["company:cybg"]
        )

        date_to = datetime.now()
        date_from = "2010-01-01"

        try:
            # Fetch events
            api_response = api_instance.events_v4_api_events_post(input_model)
            # Convert to DataFrame
            df = response_to_dataframe(api_response.dict())
            # Fill missing dates, if any
            return fill_in_missing_dates(df, date_from, date_to)
        except ApiException as e:
            print("Could not fetch response")


if __name__ == "__main__":
    event_df = get_events()
    evt_ma = event_df.columns.str.startswith("E")
    print(event_df.loc[:, evt_ma].isna().all(axis=1).mean())
    event_df.to_csv("events_df.csv")
