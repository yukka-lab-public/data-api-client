# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
import datetime
from itertools import islice

from getpass import getpass
from yl_data_api_client_wrapper import YLDataAPIClient

print("Creating new client")
client = YLDataAPIClient(token=getpass(), api_version="7-2")

portfolio = ["company:facebook", "company:apple"]
portfolio_client = client.api.portfolio
now = datetime.datetime.now()
a_week_ago = now - datetime.timedelta(days=7)

portfolio_news = portfolio_client.news(
    date_from=a_week_ago,
    date_to=now
)

print(f"{portfolio_news.news_total_count} were written in Berlin in the past week.")

# get the first 10 elements from the iterator
articles = list(islice(portfolio_news, 10))

for article in articles:
    print(article["title"])
