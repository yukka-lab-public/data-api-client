# Copyright (C) 2022 YUKKA Lab AG, Berlin - All Rights Reserved
from itertools import islice

import os
import yl_data_api_client
from yl_data_api_client_wrapper import NewsIterator
from functools import partial


def get_news():
    configuration = yl_data_api_client.Configuration(
        host="https://api.yukkalab.com/api-data/dev/9-3-5/",
        access_token=os.environ["BEARER_TOKEN"]
    )

    # Enter a context with an instance of the API client
    with yl_data_api_client.ApiClient(configuration) as api_client:
        news_instance = yl_data_api_client.NewsApi(api_client)

        get_news_fn = partial(
            news_instance.news_v2_api_displayable_type_displayable_id_news_get,
            "city",
            "berlin"
        )
        print("Fetching first batch of news")
        iterator = NewsIterator.init(get_news_fn)
        print(f"{iterator.news_total_count} were written about Berlin in the past week.")
        # get the first 10 elements from the iterator
        articles = list(islice(iterator, 10))

        for article in articles:
            print(article.title)


if __name__ == "__main__":
    get_news()
