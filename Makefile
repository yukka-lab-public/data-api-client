SHELL := /bin/bash

.EXPORT_ALL_VARIABLES:
STRICT_MODE ?= enabled
DOCKER_BUILDKIT := 1

PYTHON_SOURCE_DIR = yl_data_api_client_wrapper
PYTHON_TESTS_DIR = tests

ensure-poetry:
	python -m ensurepip
	python -m pip install --upgrade pip
	pip install poetry

python-init:
	@echo "Creating virtual environment ..."
	python --version
	poetry env use python
	@echo "Installing project into the virtual environment ..."
	poetry install --verbose --all-extras

python-lint:
	poetry run isort --check-only $(PYTHON_SOURCE_DIR) $(PYTHON_TESTS_DIR)
	poetry run black --check --diff $(PYTHON_SOURCE_DIR) $(PYTHON_TESTS_DIR)
	poetry run flake8 --jobs 4 --statistics $(PYTHON_SOURCE_DIR) $(PYTHON_TESTS_DIR)
	poetry run mypy $(PYTHON_SOURCE_DIR) $(PYTHON_TESTS_DIR)

format:
	poetry run isort $(PYTHON_SOURCE_DIR) $(PYTHON_TESTS_DIR)
	poetry run black $(PYTHON_SOURCE_DIR) $(PYTHON_TESTS_DIR)

python-test:
	poetry run pytest $(PYTHON_TESTS_DIR)

docs:
	python -m pdoc -o ./public --logo https://www.yukkalab.com/wp-content/uploads/2017/12/Logo_YUKKA_400px.png $(PYTHON_SOURCE_DIR)

test: python-lint python-test
