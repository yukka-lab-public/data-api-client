## yl-data-api-client Jupyter Notebooks 

A series of Jupyter notebooks that show more in-depth examples of how YUKKA data API signals (as acquired using `yl-data-api-client`) can be used.
