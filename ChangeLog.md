[Unreleased]

## Version [6.0.0]

- rename `yl_data_api_client` to `yl_data_api_client_wrapper` to reflect that we have an automated Data API client and this is the wrapper for this client
- use the automatically generated model definitions from `yl_data_api_client` instead of the custom `yl_data_api_schema` package

## Version [5.0.5]

- add `py.typed` (typed Data API client)
- implement `next` for NewsIterator making it a proper `Iterable`

## Version [5.0.0]

- migrate to Poetry
- make pandas mandatory
- update yl-data-api-schema
- fixes in news iterator

## Version [4.0.0]

- start incorporating data api schema into client [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/16)

## Version [3.0.13]

- fix bug in `ml_score_ts` and `ml_score_ts_itemized` 

## Version [3.0.12]

- Allow passing extra headers [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/17)

## Version [3.0.11]

- Allow ignoring the SSL certificate [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/15)

## Version [3.0.10]

- Allow using the Data API Client behind a proxy [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/14)

## Version [3.0.9]

- Add typing marker `py.typed` to make the library `mypy`-compliant [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/12)

## Version [3.0.8]

- ensure `YLDataAPIClient` works on stage. [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/11)

## Version [3.0.7]

- Make `pandas` an optional dependency for the YLDataAPIClient
- Fix typing hints for `ts_resp2dataframe` and `events_ts_resp2dataframe`. [MR](https://gitlab.com/yukka-lab-public/data-api-client/-/merge_requests/10/)

## Version [3.0.6]

- fix bull/bear score_ts test

## Version [3.0.5]

- ensure `score_ts`, `events_ts` and other timeseries endpoints work when we don't specify `date_from` and `date_to`.

## Version [3.0.4]

- When we get 401 errors and `return_resp=False`, raise an error in the client instead of forging ahead and getting inscrutable `KeyError`s. This is especially applicable to timeseries endpoints that return dataframe objects. 
- Do a little refactoring to have consistent error handling in the client

## Version [3.0.3]

- add explicit 2.x to 3.x migration guide in documentation 
- fix bug in documentation generation in CI configuration

## Version [3.0.2]

- the calls to the `news` endpoints should now respect the requested `size` parameter

## Version [3.0.1]

- fix bug where we couldn't process custom weights for `score_ts_top_events` endpoint 

## Version [3.0.0]

Two breaking changes:

- We can no longer do `client.company.tesla_motors`, we have to do `client.api.company.tesla_motors`.
- The expected custom_weights data structure (for `score_ts` and `score_ts_itemized`) is different.

Various improvements/simplifications in the codebase:

- Add `return_resp` kwarg to `YLDataAPIClient` constructor to allow returning `Response` object for all subsequent requests
- get rid of references to "old" and "new" format API; everything has been unified
- add support for 8.1 data release
- add support for `*itemized` timeseries endpoints.

## Version [2.5.4]

- fix bug in `events_ts`

## Version [2.5.3]

- fix bug in `fill_in_missing_dates`; add associated unit tests

## Version [2.5.2]

- update configuration so 7.2 prod uses "new" data API format 
- fix bug in events_ts where `fill='full'` wouldn't work correctly

## Version [2.5.1]

- YLDataAPICLient will not raise errors if we get something other than a 200 from data API.
  - This means that if we pass `return_resp` to an endpoint function, we won't see an error if we get a 4xx or 50x error from the data API 
  - errors are still raised if we don't use `return_resp`
- fix bug where we couldn't call `client.misc.entity.portfolio()` 

## Version [2.5.0]

- general refactor of codebase 
- we can now "preload" post entities with data: `client.api.portfolio(["company:yukka",...]).score_ts` instead of `client.api.portfolio.score_ts(data=["company:yukka", ...])`. The latter syntax is still supported. 
- broader, and more general range of support for YUKKA data API endpoints. 
- NewsIterator is indexable 
- fix bug in events_ts where we wouldn't get all the event counts for portfolios


[Unreleased]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/6.0.0...master
[6.0.0]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/5.0.5...6.0.0
[5.0.5]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/5.0.0...5.0.5
[5.0.0]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/4.0.0...5.0.0
[4.0.0]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.13...4.0.0
[3.0.13]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.12...3.0.13
[3.0.12]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.11...3.0.12
[3.0.11]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.10...3.0.11
[3.0.10]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.9...3.0.10
[3.0.9]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.8...3.0.9
[3.0.8]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.7...3.0.8
[3.0.7]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.6...3.0.7
[3.0.6]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.5...3.0.6
[3.0.5]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.4...3.0.5
[3.0.4]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.3...3.0.4
[3.0.3]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.2...3.0.3
[3.0.2]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.1...3.0.2
[3.0.1]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/3.0.0...3.0.1
[3.0.0]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/2.5.4...3.0.0
[2.5.4]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/2.5.3...2.5.4
[2.5.3]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/2.5.2...2.5.3
[2.5.2]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/2.5.1...2.5.2
[2.5.1]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/2.5.0...2.5.1
[2.5.0]: https://gitlab.com/yukka-lab-public/data-api-client/-/compare/2.4.16...2.5.0
